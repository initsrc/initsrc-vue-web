import commonRequest from '@/api/module/commonRequest.js';
import baseRequest from '@/api/module/baseRequest.js';
// 其他模块的接口……

// 导出接口
export default {
  commonRequest,
  baseRequest,
  // ……
}
