export const basicComponents = [{
  type: 'input',
  icon: 'icon-input',
  options: {
    title: "单行输入框",
    placeholder: "请输入",
    defaultValue: "",
    required: false,
    pattern: '',
    dataType: "",
    limit: 1,
    isScan: false,
    isPrint: true
  }
}, {
  type: 'textarea',
  icon: 'icon-textarea',
  options: {
    title: "多行输入框",
    placeholder: "请输入",
    defaultValue: "",
    required: false,
    pattern: '',
    limit: 150,
    isPrint: true
  }
}, {
  type: 'number',
  icon: 'icon-number',
  options: {
    title: "数字输入框",
    placeholder: "请输入",
    defaultValue: "",
    controlsPosition: "",
    min: 0,
    max: 1,
    required: false,
    isPrint: true
  }
}, {
  type: 'select',
  icon: 'icon-textarea',
  options: {
    title: "选择器",
    placeholder: "请选择",
    defaultValue: "",
    required: false,
    isPrint: true,
    options: [{
        value: 'Option 1',
        label: 'Option 1'
      },
      {
        value: 'Option 2',
        label: 'Option 2'
      },
      {
        value: 'Option 3',
        label: 'Option 3'
      },
    ],
  }
}, {
  type: 'radio',
  icon: 'icon-textarea',
  options: {
    title: "单选框",
    placeholder: "请选择",
    defaultValue: "",
    required: false,
    isPrint: true,
    options: [{
        value: 'Option 1',
        label: 'Option 1'
      },
      {
        value: 'Option 2',
        label: 'Option 2'
      },
      {
        value: 'Option 3',
        label: 'Option 3'
      },
    ],
  }
}, {
  type: 'checkbox',
  icon: 'icon-check-box',
  options: {
    title: "多选框",
    placeholder: "请选择",
    defaultValue: [],
    required: false,
    isPrint: true,
    options: [{
        value: 'Option 1',
        label: 'Option 1'
      },
      {
        value: 'Option 2',
        label: 'Option 2'
      },
      {
        value: 'Option 3',
        label: 'Option 3'
      }
    ],
  }
}, {
  type: 'date',
  icon: 'icon-textarea',
  options: {
    title: "日期",
    placeholder: "请选择",
    defaultValue: "",
    required: false,
    isPrint: true,
    format: 'yyyy-MM-dd',
  }
}, ]
